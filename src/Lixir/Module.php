<?php

namespace Lixir\Dummy;

use Illuminate\Support\ServiceProvider;

class Module extends ServiceProvider
{
    public function boot()
    {
        $this->publishFiles();
    }

    public function register()
    {
    }

    public function publishFiles()
    {
        $this->publishes([__DIR__ . '/../assets' => storage_path('app/public')], 'public');
    }
}
